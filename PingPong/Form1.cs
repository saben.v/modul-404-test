﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        private int directionX = 5;
        private int directionY = 2;

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Spielstarten(object sender, EventArgs e)
        {
            tmrSpiel.Start();
            tmrSpiel.Enabled = true;
        }

        private void InitializeMyScrollBar()
        {
            VScrollBar vScrollBar1 = new VScrollBar();
            
            vScrollBar1.Dock = DockStyle.Right;
            
            Controls.Add(vScrollBar1);
        }

        private void tmrSpiel_Tick_1(object sender, EventArgs e)
        {
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X + 0, 0 +(Convert.ToInt32(vScrollBar1.Value)* 248 / 100));
            picBall.Location = new Point(picBall.Location.X + directionX, picBall.Location.Y + directionY);
            
            if (picBall.Bounds.IntersectsWith(picSchlägerRechts.Bounds))
            {
                directionX = -directionX;
                
            }

            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width)
            {
                directionX = -directionX;
            }

            if (picBall.Location.X <= 0)
            {
                directionX = -directionX;
            }

            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                directionY = -directionY;
            }

            if (picBall.Location.Y < 0)
            {
                directionY = -directionY;
            }
        }
    }
}
