﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Taschenrechner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddition_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 + zahl2;
            lblErgebnis.Text =Convert.ToString(ergebnis);
            lblOperator.Text = "+";
        }

        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 - zahl2;
            lblErgebnis.Text =Convert.ToString(ergebnis);
            lblOperator.Text = "-"; 
        }

        private void btnMittelwert_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtOperand1.Text.ToString());
            double b = Convert.ToDouble(txtOperand2.Text.ToString());
            lblErgebnis.Text = Convert.ToString((a + b) / 2.0);
            lblOperator.Text = "X̅";
        }

        private void btnpotenz_Click(object sender, EventArgs e)
        {
            for (int x = 0; x <= 100; x++)
            {
                double y = Math.Pow(3, x);
                double z = Math.Pow(3, (x + 1));
                if ((Convert.ToDouble(txtOperand1.Text) > y) & (Convert.ToDouble(txtOperand2.Text) <= z))
                {
                    lblErgebnis.Text = Convert.ToString(x+1);
                }
                if (Convert.ToDouble(txtOperand1.Text) == y)
                {
                    lblErgebnis.Text = Convert.ToString(x);
                }
                if (Convert.ToDouble(txtOperand2.Text) == z)
                {
                    lblErgebnis.Text = Convert.ToString(x + 1);
                }
                lblOperator.Text = "^";
            }
        }

        private void btnMaximum_Click(object sender, EventArgs e)
        {
            int[] array1 = { 1, -1, -2, 0 };
            
            Console.WriteLine(array1.Max());
            
            Console.WriteLine(array1.Max(element => Math.Abs(element)));
            lblErgebnis.Text = Convert.ToString(array1.Max(element => Math.Abs(element)));
            lblOperator.Text = "max";
        }

        private void btnMultiplikation_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 * zahl2;
            lblErgebnis.Text =Convert.ToString(ergebnis);
            lblOperator.Text = "x";
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            double ergebnis = zahl1 / zahl2;
            lblErgebnis.Text =Convert.ToString(ergebnis);
            lblOperator.Text = "/";
        }
    }
}